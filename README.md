# **Graffiti iOS Xcode Project** #
________________________________________________________
## **General:** ##

* This update contains modified Objective-c code (Xcode level).
* Runnable Xcode iOS Web App base project.
* This is a complete iOS project, therefore it's totally fine to run this project independently.


## **For your test purpose:** ##

For your convince, please replace everything under "ios" folder at the following directory with this repository before compile.
```

<YOUR_PATH_TO_GRAFFITI_PROJECT>/platforms/ios/

```

## **References:** ##

* [Xcode Concepts](https://developer.apple.com/library/ios/featuredarticles/XcodeConcepts/Concept-Projects.html)

* [Getting Started in Simulator](https://developer.apple.com/library/prerelease/ios/documentation/IDEs/Conceptual/iOS_Simulator_Guide/GettingStartedwithiOSSimulator/GettingStartedwithiOSSimulator.html#//apple_ref/doc/uid/TP40012848-CH5-SW1)

* [Launching Your App on Devices](https://developer.apple.com/library/mac/documentation/IDEs/Conceptual/AppDistributionGuide/LaunchingYourApponDevices/LaunchingYourApponDevices.html#//apple_ref/doc/uid/TP40012582-CH27-SW1)

________________________________________________________
Updated on Nov 12th, Thu 9:57 PM